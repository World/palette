<a href="https://flathub.org/apps/details/org.gnome.design.Palette">
<img src="https://flathub.org/assets/badges/flathub-badge-i-en.png" width="190px" />
</a>

# Colour Palette

<img src="https://gitlab.gnome.org/World/design/palette/raw/master/data/org.gnome.design.Palette.svg" width="128" height="128" />
<p>Colour Palette tool.</p>

## Screenshots

<div align="center">
![screenshot](data/screenshots/screenshot1.png)
</div>

## Hack on Colour Palette
To build the development version of Colour Palette and hack on the code
see the [general guide](https://wiki.gnome.org/Newcomers/BuildProject)
for building GNOME apps with Flatpak and GNOME Builder.

You are expected to follow the GNOME Code of Conduct when participating in project
spaces.



